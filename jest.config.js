const config = {
    reporters: [ "default", "jest-junit" ],
    verbose: true,
};
  
module.exports = config;