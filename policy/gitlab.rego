package main

deny[msg] {
  # access the build.image section of a GitLab Pipeline file
  x:= input.unit_testing.image
  # get the boolean value of the string of the image to see it is http
  b := startswith(x, "http")
  # if it doesn't start with http, assume they are access docker.io
  b == false 
  # print a failure message with details from above
  msg := sprintf("docker.io images are not approved: %q", [x])
}